//
//  AppDelegate.swift
//  ituneSearcher
//
//  Created by nwfmbin2 on 2021/07/01.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {


    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        if #available(iOS 13.0, *){
        }else{
            guard var window = window else { return true}
            window = UIWindow(frame: UIScreen.main.bounds)
            
            let vc = SearchViewController()
            let navC = MainNavigationController(rootViewController: vc)
            window.rootViewController = navC

            // 將 UIWindow 設置為可見的
            window.makeKeyAndVisible()
        }
        return true
    }



}

