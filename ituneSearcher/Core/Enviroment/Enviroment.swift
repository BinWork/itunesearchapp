//
//  Enviroment.swift
//  ituneSearcher
//
//  Created by nwfmbin2 on 2021/07/01.
//

import Foundation


class Enviroment{
    
    static let shared = Enviroment()
    
    let domain: String = "https://itunes.apple.com"
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}







extension Enviroment{
    
    
    func call<T: Decodable>(
        api: APIs,querys: [String: String], myStruct: T.Type, completion:
        @escaping (Result< T, RequestError>) -> Void) {
        
        showLoadingHud()
        
        let domainUrl = Enviroment.shared.domain + api.url
        var urlComponents = URLComponents(string: domainUrl)
        urlComponents?.add(querys: querys)
        
        guard let url = urlComponents?.url else {
            completion(.failure(.InValidURL(api.url)))
            hideLoadingHud()
            return
        }
    
        makeHTTPRequestWithStruct(with: url, myStruct: myStruct.self) { [weak self]
            (result) in
            guard let sf = self else { return }
            switch result{
            case .success(let s):
                completion(.success(s))
            case .failure(let err):
                completion(.failure(err))
            }
            sf.hideLoadingHud()
        }
    
    }
    
    
    
    private func makeHTTPRequestWithStruct<T: Decodable>(
        with url: URL, myStruct: T.Type , completion:
        @escaping (Result< T, RequestError>) -> Void){
        
        let task = URLSession.shared.dataTask(with: url) {
            (requestData, requestRes, requestErr) in

            if let err =  requestErr{
                completion(.failure(.RequestError(err)))
                return
            }

            guard let data = requestData else{
                completion(.failure(.DataIsNil))
                return
            }
            
            do{
                let json = try JSONDecoder().decode(myStruct.self, from: data)
                completion(.success(json))
            }catch{
                completion(.failure(.ParsingFailed))
            }
            
        }

        task.resume()
    }
    
}










extension Enviroment{
    
    func showLoadingHud(){
        //track("顯示loading hud")
    }
    
    func hideLoadingHud(){
        //track("隱藏loading hud")
    }
    
}
