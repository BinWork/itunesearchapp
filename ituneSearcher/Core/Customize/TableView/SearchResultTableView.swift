//
//  SearchResultTableView.swift
//  ituneSearcher
//
//  Created by nwfmbin2 on 2021/07/01.
//

import UIKit

class SearchResultTableView: UITableView {

    var keyword: String?
    
    var models: [SearchResultStruct.Result]?{
        didSet{
            reloadData()
        }
    }
    
    private let cellClass = SearchResultTableViewCell.self
    private let cellID = SearchResultTableViewCell.cellId
    private let cellHeight: CGFloat = CGFloat(80)
    
    override init(frame: CGRect, style: UITableView.Style) {
        super.init(frame: frame, style: style)
        
        backgroundColor = .clear
        showsVerticalScrollIndicator = false
        
        setProperties(dataSource: self, delegate: self,
                      cellClass: cellClass, cellReuseId: cellID)
    }
    
    
    func setProperties(dataSource ds: UITableViewDataSource?, delegate dg: UITableViewDelegate?, cellClass: AnyClass?, cellReuseId: String?) {
        dataSource = ds
        delegate = dg
        guard let cs = cellClass,
              let id = cellReuseId else { return }
        register(cs, forCellReuseIdentifier: id)
    }
    
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    func setModels(with newModels: [SearchResultStruct.Result]?) {
        models = newModels
    }
    
    
}



extension SearchResultTableView: UITableViewDataSource{
  
    func numberOfSections(in tableView: UITableView) -> Int {
        return max(models?.count ?? 1, 1)
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if models?.count == 0{
            let cell = NoDataTableViewCell()
            cell.setKeyword(with: keyword)
            return cell
        }
        
        
        if let cell = tableView.dequeueReusableCell(
            withIdentifier: SearchResultTableViewCell.cellId, for: indexPath)
            as? SearchResultTableViewCell{
            
            if let m = models?[indexPath.section]{
                cell.setModel(with: m)
            }
            return cell
        }
        return UITableViewCell()
    }
    
}


extension SearchResultTableView: UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        guard
            let parentVC = superview?.parentViewController,
            let navC = parentVC.navigationController,
            let m = models?[safe:indexPath.section]
            else { return }
        
        let playerVC = PreviewViewController(result: m)
        
        navC.pushViewController(playerVC, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return cellHeight
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 10
    }
}
