//
//  SearchResultTableViewCell.swift
//  ituneSearcher
//
//  Created by nwfmbin2 on 2021/07/01.
//

import UIKit
import SDWebImage

class SearchResultTableViewCell: UITableViewCell {

    static let cellId = String(describing: self)
    
    var model: SearchResultStruct.Result?{
        didSet{
            updateUI()
        }
    }
    
    let imgAlbum: UIImageView = {
        let v = UIImageView()
        v.contentMode = .scaleAspectFit
        return v
    }()
    
    
    lazy var stackView: UIStackView = {
        return genStackView()
    }()
    
    let lblSongName: UILabel = {
        let lbl = UILabel()
        lbl.textColor = .white
        return lbl
    }()
    
    
    let lblArtist: UILabel = {
        let lbl = UILabel()
        lbl.textColor = .white
        return lbl
    }()
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)

        selectionStyle = .none
        backgroundColor = .clear
        addUI()
        
    }
    
    private func addUI(){
        
        contentView.addSubview(imgAlbum)
        imgAlbum.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(snpOffset.top)
            make.left.equalToSuperview().offset(snpOffset.left)
            make.bottom.equalToSuperview().offset(snpOffset.bottom)
            make.width.equalTo(imgAlbum.snp.height)
        }
        
        contentView.addSubview(stackView)
        stackView.snp.makeConstraints { (make) in
            make.top.bottom.equalTo(imgAlbum)
            make.right.equalToSuperview().offset(snpOffset.right)
            make.left.equalTo(imgAlbum.snp.right).offset(30)
        }
        
    }
    
    func setModel(with myModel: SearchResultStruct.Result) {
        model = myModel
    }
    
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

extension SearchResultTableViewCell{
    
    private func genStackView() -> UIStackView{
        let s = UIStackView()
        s.set(Axis: .vertical, Spacing: 0, Distribution: .fillEqually)
        s.addArrangedSubview(lblSongName)
        s.addArrangedSubview(lblArtist)
        
        return s
    }
    
    
    func updateUI(){
        guard let m = model else { return }
        if let urlAlbum = m.artworkUrl60{
            imgAlbum.sd_setImage(with: URL(string: urlAlbum), completed: nil)
        }
        
        if let songName = m.trackName{
            lblSongName.text = songName
        }else if let censoredName = m.trackCensoredName{
            lblSongName.text = censoredName
        }
        
        if let artist = m.artistName{
            lblArtist.text = artist
        }
    }
    
}
