//
//  NoDataTableViewCell.swift
//  ituneSearcher
//
//  Created by nwfmbin2 on 2021/07/01.
//

import UIKit

class NoDataTableViewCell: UITableViewCell {

    static let cellId = String(describing: self)
    
    var keyword: String? = nil{
        didSet{
            if let k = keyword{
                lblRemind.text = "Can't find retults of '\(k)...'"
            }else{
                lblRemind.text = "Try search something"
            }
        }
    }

    let lblRemind: UILabel = {
        let lbl = UILabel()
        lbl.textColor = .white
        lbl.textAlignment = .center
        lbl.adjustsFontSizeToFitWidth = true
        lbl.minimumScaleFactor = 0.3
        return lbl
    }()
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)

        selectionStyle = .none
        backgroundColor = .clear
        addUI()
        
    }
    
    private func addUI(){
        
        contentView.addSubview(lblRemind)
        lblRemind.snp.makeConstraints { (make) in
            make.top.left.right.bottom.equalToSuperview()
        }
        
    }
    
    func setKeyword(with k: String?) {
        keyword = k
    }
    
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
