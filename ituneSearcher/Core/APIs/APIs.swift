//
//  APIs.swift
//  ituneSearcher
//
//  Created by nwfmbin2 on 2021/07/01.
//

import Foundation


enum APIs{
    case search
    case anotherAPI_1
    case anotherAPI_2
    
    
    var url: String{
        switch self {
        case .search:
            return "/search"
        default:
            return "/search"
        }
    }
}
