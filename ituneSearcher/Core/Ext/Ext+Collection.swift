//
//  Ext+Collection.swift
//  ituneSearcher
//
//  Created by nwfmbin2 on 2021/07/01.
//

import Foundation

extension Collection {
    subscript (safe index: Index) -> Element? {
        return indices.contains(index) ? self[index] : nil
    }
}
