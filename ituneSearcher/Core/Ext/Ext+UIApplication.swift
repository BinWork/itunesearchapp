//
//  Ext+UIApplication.swift
//  ituneSearcher
//
//  Created by nwfmbin2 on 2021/07/01.
//

import Foundation
import UIKit

/// 取得當下正在顯示的ViewController
extension UIApplication{
    /// 取得當下正在顯示的ViewController
    var visibleViewController: UIViewController? {

        guard let rootViewController = keyWindow?.rootViewController else {
            return nil
        }

        return getVisibleViewController(rootViewController)
    }

    private func getVisibleViewController(_ rootViewController: UIViewController) -> UIViewController? {

        if let presentedViewController = rootViewController.presentedViewController {
            return getVisibleViewController(presentedViewController)
        }

        if let navigationController = rootViewController as? UINavigationController {
            return navigationController.visibleViewController
        }

        if let tabBarController = rootViewController as? UITabBarController {
            return tabBarController.selectedViewController
        }

        return rootViewController
    }
}
