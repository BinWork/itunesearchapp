//
//  Ext+UIStackView.swift
//  ituneSearcher
//
//  Created by nwfmbin2 on 2021/07/01.
//

import UIKit.UIStackView
extension UIStackView{
    
    /// 配置UIStackView
    /// - Parameters:
    ///   - Axis: 水平 or 垂直
    ///   - Spacing: 間隔
    ///   - Distribution: 分佈種類
    func set(Axis: NSLayoutConstraint.Axis,Spacing: CGFloat,
             Distribution: UIStackView.Distribution){
        axis = Axis
        spacing = Spacing
        distribution = Distribution
    }
}
