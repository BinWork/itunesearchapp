//
//  Ext+Url.swift
//  ituneSearcher
//
//  Created by nwfmbin2 on 2021/07/01.
//

import Foundation

extension URLComponents{
    
    mutating func add(querys: [String: String]){
        var items: [URLQueryItem] = []
        querys.forEach { (key, value) in
            let v = value.replacingOccurrences(of: " ", with: "+")
            items.append(URLQueryItem(name: key, value: v))
        }
        queryItems = items
    }
    
}
