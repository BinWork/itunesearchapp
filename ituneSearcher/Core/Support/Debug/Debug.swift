//
//  Debug.swift
//  ituneSearcher
//
//  Created by nwfmbin2 on 2021/07/01.
//

import Foundation

public func track(_ obj: Any? = nil,
                  file: String = #file, function: String = #function, line: Int = #line ) {
    print("=====================")
    print("[file] \(URL(fileURLWithPath: file).lastPathComponent)")
    print("[row] \(line)")
    print("[func] \(function)")
    if let arr = obj as? [AnyObject] {
        arr.forEach({
            print($0)
        })
    }else if let arr = obj as? [Any] {
        arr.forEach({
            print($0)
        })
    }else if let dict = obj as? Dictionary<String, Any>{
        dict.forEach({
            print("[\($0.key)]: \($0.value)")
        })
    }else if let str = obj as? String{
        print(str)
    }else{
        print(obj as Any)
    }
    print("=====================\n\n\n")
}
