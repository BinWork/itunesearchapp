//
//  ErrorEnum.swift
//  ituneSearcher
//
//  Created by nwfmbin2 on 2021/07/01.
//

import Foundation
import UIKit

enum RequestError: Error{
    case InValidURL(String)
    case ConnectionProblem(String)
    case ParsingFailed
    case RequestError(Error)
    case ResponseIsNil
    case DataIsNil
    
    func popAlert(){
        
        DispatchQueue.main.async {
            if let vc = UIApplication.shared.visibleViewController{

                let alert = UIAlertController(title: "\(self)", message: nil, preferredStyle: .alert)
                let cancel = UIAlertAction(title: "確定", style: .default, handler: nil)
                alert.addAction(cancel)
                vc.present(alert, animated: true, completion: nil)
            }else{
                track("找不到visibleController")
            }
        }
    }
}
