//
//  PreviewViewController.swift
//  ituneSearcher
//
//  Created by nwfmbin2 on 2021/07/01.
//

import UIKit
import AVFoundation

class PreviewViewController: BaseViewController {

    var result: SearchResultStruct.Result?
    
    var songPlayer: AVPlayer?
    
    let imgAlbum: UIImageView = {
        let v = UIImageView()
        v.contentMode = .scaleAspectFit
        return v
    }()
    
    let textView: UITextView = {
        let v = UITextView()
        v.textColor = .white
        v.textAlignment = .center
        return v
    }()
    
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        imgAlbum.layer.masksToBounds = true
        imgAlbum.layer.cornerRadius = imgAlbum.frame.height / 2
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if ((navigationController?.navigationBar.isHidden) != nil){
            navigationController?.navigationBar.isHidden = false
        }
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.navigationBar.isHidden = false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()


        addUI()
        updateResult()
        updateDescription()
        playSong()
    }
    
    init(result: SearchResultStruct.Result) {
        self.result = result
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    
    private func addUI(){
        
        view.addSubview(imgAlbum)
        imgAlbum.snp.makeConstraints { (make) in
            make.center.equalToSuperview()
            make.width.equalToSuperview().multipliedBy(0.7)
            make.height.equalTo(imgAlbum.snp.width)
        }
        
        view.addSubview(textView)
        textView.snp.makeConstraints { (make) in
            make.top.equalTo(imgAlbum.snp.bottom).offset(snpOffset.top * 2)
            make.bottom.equalToSuperview().offset(snpOffset.bottom * 2)
            make.width.equalToSuperview().multipliedBy(0.7)
            make.centerX.equalToSuperview()
        }
        
    }
    

}



extension PreviewViewController{
    
    private func updateResult(){
        guard let r = result else { return }
        if let url = r.artworkUrl100{
            imgAlbum.sd_setImage(with: URL(string: url), completed: nil)
        }
        
    }
    
    private func updateDescription(){
        
        guard let r = result else { return }
        let songName = r.trackName ?? r.trackCensoredName ?? ""
        let artist = r.artistName ?? ""
        let collectionName = r.collectionName ?? ""
        let pricePrefix = r.currency ?? ""
        let price = String(r.trackPrice ?? 0.0)
        
        let str: String = """
        \(artist) - \(songName)
        \(collectionName)
        \(pricePrefix) \(price)
        """
        
        textView.text = str
    }
    
}





















extension PreviewViewController{
    
    
    
    private func playSong(){
        
        guard let strUrl = result?.previewUrl,
              let url = URL(string: strUrl) else { return }
        
        
        do{
            
            songPlayer = AVPlayer(url: url)
            songPlayer?.volume = 1
            songPlayer?.play()
        }catch let err{
            track(err)
        }
    }
    
    
}

