//
//  SearchViewController.swift
//  ituneSearcher
//
//  Created by nwfmbin2 on 2021/07/01.
//

import UIKit
import SnapKit

class SearchViewController: BaseViewController {

    private var tempKeyword: String? = nil
    
    lazy var input: UITextField = {
       let tf = UITextField()
        tf.textColor = .white
        tf.backgroundColor = .gray
        tf.clearButtonMode = .whileEditing
        tf.font = .systemFont(ofSize: 20)
        tf.placeholder = "find some song..."
        tf.returnKeyType = .search
        tf.delegate = self
        return tf
    }()
    
    let tbResults: SearchResultTableView = {
        let tb = SearchResultTableView()
        return tb
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addUI()
        
        search(keyword: "bad romance")
    }
    
    private func addUI(){
        
        view.addSubview(input)
        input.snp.makeConstraints { (make) in
            if #available(iOS 11.0, *) {
                make.top.equalTo(view.safeAreaLayoutGuide).offset(snpOffset.top * 2)
            } else {
                make.top.equalTo(45)
            }
            make.left.equalToSuperview().offset(snpOffset.left)
            make.right.equalToSuperview().offset(snpOffset.right)
            make.height.equalTo(30)
        }
        
        view.addSubview(tbResults)
        tbResults.snp.makeConstraints { (make) in
            make.top.equalTo(input.snp.bottom).offset(snpOffset.top * 3)
            make.left.right.equalTo(input)
            make.bottom.equalToSuperview().offset(snpOffset.bottom * 3)
        }
    }

    

}

extension SearchViewController{
    
    func search(keyword term: String?){
        guard let t = term, t != "" else { return }
        
        Enviroment.shared.call(api: .search, querys: ["term": t], myStruct: SearchResultStruct.self) { [weak self] (result) in
            guard let sf = self else { return }
            switch result{
            case .failure(let err):
                track(err)
            case .success(let s):
                DispatchQueue.main.async {
                    let resultsFiltered = s.results?.filter({ (result) -> Bool in
                        if let kind = result.kind,
                           kind == "song"{
                            return true
                        }
                        return false
                    })
                    sf.tbResults.keyword = term
                    sf.tbResults.setModels(with: resultsFiltered)
                    sf.tbResults.setContentOffset(.zero, animated: true)
                }
            }
        }
    }
    
}

extension SearchViewController: UITextFieldDelegate{
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if let keyword = textField.text{
            if tempKeyword != keyword{
                tempKeyword = keyword
                search(keyword: keyword)
            }
        }
        view.endEditing(true)
        return true
    }
    
}
